trigger AccountTrigger on Account (after insert, after update) {
    
    if (Trigger.isAfter) {
        if (Trigger.isInsert || Trigger.IsUpdate) {
            BusinessCardServices.assignAccountToCard(
                AccountServices.getAccountNameToAccountId(
                	AccountServices.filterAccountsWithChangedNames(Trigger.New, null)));
        }
    }
}