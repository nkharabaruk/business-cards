trigger BusinessCardTrigger on Scanned_Business_Card__c (before update) {
	
    if (Trigger.isBefore) {
        if (Trigger.isUpdate) {
            System.debug(Trigger.New);
            BusinessCardServices.assignAccountToCard(BusinessCardServices.filterCardsWithChangedAccountNames(Trigger.New, Trigger.OldMap));
            BusinessCardServices.assignContactToCard(BusinessCardServices.filterCardsWithChangedFirstOrLastNames(Trigger.New, Trigger.OldMap));
            System.debug(Trigger.New);
        }   
    }
}