public class AccountServices {

    public static List<Account> selectAccountsByName(Set<String> accountNames) {
        return [SELECT Name, CreatedDate 
                FROM Account 
                WHERE Name IN :accountNames 
                ORDER BY CreatedDate DESC];
    }
    
    public static List<Account> filterAccountsWithChangedNames(List<Account> accounts, Map<Id, Account> accountsOld) {
        List<Account> accountsWithChangedName = new List<Account>();
        for (Account account : accounts) { 
            if (accountsOld != null) {
                if (accountsOld.get(account.Id).Name == account.Name) continue;
            }
            accountsWithChangedName.add(account);
        }
        return accountsWithChangedName;
    }
    
    public static Map<String, Id> getAccountNameToAccountId (List<Account> accounts) {
        Map<String, Id> accountNameToAccountId = new Map<String, Id>();
        for (Account account : accounts) {
            if (accountNameToAccountId.get(account.Name) != null) continue;
            accountNameToAccountId.put(account.Name, account.Id);
        }
        return accountNameToAccountId;
    }
}