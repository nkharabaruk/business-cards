global class ExtendedResponseHandler extends ResponseHandler {
    
    public List<Map<String, String>> Data {get;set;}
    
    public class NoRecordMatchException extends Exception {}
    public class InvalidFileException extends Exception {}
}