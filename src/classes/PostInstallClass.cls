global class PostInstallClass implements InstallHandler {
    global void onInstall(InstallContext context) {
        if (context.previousVersion() == null) {
            Attachment_Settings__c attachmentSettings = Attachment_Settings__c.getInstance();
            insert(attachmentSettings);
            
            External_ID_Settings__c externalIDSettings = External_ID_Settings__c.getInstance();
            insert(externalIDSettings);
        }
    }
}