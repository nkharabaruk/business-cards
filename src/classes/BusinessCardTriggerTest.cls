@isTest
public class BusinessCardTriggerTest {
	
	static testMethod void testAccountLookupAfterUpdate() {
		Account account = new Account(Name = 'Jane Austen');
        insert account;
        Scanned_Business_Card__c card = new Scanned_Business_Card__c(Account_Name__c = 'Jane Austen (will be edit)', Created_By_Id__c = 'someId');
        insert card;
        
        card.Account_Name__c = 'Jane Austen';

        test.startTest();
            try {
                update card;
                System.assert( true );
            } catch( Exception e ) {
                System.assert( false );
            }
        test.stopTest();
        
        card = [SELECT Account__c FROM Scanned_Business_Card__c WHERE Id = :card.Id];
        
        System.assertEquals(account.Id, card.Account__c);
    }
    
    static testMethod void testContactLookupAfterUpdate() {
		Contact contact = new Contact(FirstName = 'Jane', LastName = 'Austen');
        insert contact;
        Scanned_Business_Card__c card = new Scanned_Business_Card__c(First_Name__c = 'Not Jane', Last_Name__c = 'Austen', Created_By_Id__c = 'someId');
        insert card;
        
        System.assertNotEquals(contact.Id, card.Contact__c);
        card.First_Name__c = 'Jane';

        test.startTest();
            try {
                update card;
                System.assert( true );
            } catch( Exception e ) {
                System.assert( false );
            }
        test.stopTest();
        
        card = [SELECT Contact__c FROM Scanned_Business_Card__c WHERE Id = :card.Id];
        
        System.assertEquals(contact.Id, card.Contact__c);
    }
}