@RestResource(urlMapping='/card/*/image')
global class ImageService {
    
    private static Attachment_Settings__c getAttachmentSettings() {
		Attachment_Settings__c attachmentSettings = new Attachment_Settings__c(Business_Card_Suffix__c = 'BusinessCard');
        return attachmentSettings;
    }
    
    @HttpGet 
    global static ResponseHandler GET() {
        ResponseHandler response = new ResponseHandler();
        Attachment image = getImageByCardId();
        
        if (image != null) {
            response.Status = Label.Success_Status;
            
            List<Map<String, String>> objects = new List<Map<String, String>>();
            Map<String, String> objectFields = new Map<String, String>();
            objectFields.put('Id', image.Id);
            objectFields.put('Name', image.Name);
            objectFields.put('Body', EncodingUtil.base64Encode(image.Body));
            objects.add(objectFields);
            
            response.Data = objects;
            response.Message = Label.Success_Status + ' : ' + Label.Found_Image;
        } else {
            response.Status = Label.Error_Status;
            response.Message = Label.Not_Found_Image;
        }
        
        return response;
    }
    
    private static Attachment getImageByCardId() {
		String cardId = getCardIdFromURL();
        List<Attachment> images;
        try {
            images = [SELECT Name, Body FROM Attachment WHERE ParentId = : cardId]; 
        } catch(System.QueryException e) {
            throw new ResponseHandler.NoRecordMatchException('Unable to find image with cardId : ' + cardId);
        } 
        
        return images[0];        
    }
    
    private static String getCardIdFromURL() {
        String cardId = RestContext.request.requestURI.substringBetween('card/', '/image');
        if (!CardService.isNotNullOrEmpty(cardId)) throw new ResponseHandler.NoRecordMatchException('Unable to find records maching Id : ' + cardId);
        return cardId;
    }
    
    private static String getFileExtension() {
		String fileContentType = RestContext.request.params.get('Content-Type');
        if (fileContentType.split('/')[0] != 'image') {
            throw new ResponseHandler.InvalidFileException('Invalid file. You cannot upload: ' + fileContentType.split('/')[0]);
        } 
        return fileContentType.split('/')[1];
    }
    
    @HttpPost
    global static ResponseHandler POST() {
        ResponseHandler response = new ResponseHandler();
        String fileExtension = RestContext.request.params.get('Content-Type');

        Attachment image = createImage(fileExtension);
        
        try {
            insert image;
            List<Map<String, String>> objects = new List<Map<String, String>>();
            Map<String, String> objectFields = new Map<String, String>();
            objectFields.put('Id', image.Id);
            objectFields.put('Name', image.Name);
            objectFields.put('Body', EncodingUtil.base64Encode(image.Body));
            objects.add(objectFields);
            
            response.Data = objects;
            response.Status = Label.Success_Status;
            response.Message = Label.Success_Status + ' : ' + Label.Found_Image;
        } catch (DmlException e) {
            response.Status = Label.Error_Status;
            response.Message = e.getMessage();
        }
        
        return response;   
    }
    
    private static Attachment createImage(String fileExtension) {
        Scanned_Business_Card__c card = CardService.getCardById(getCardIdFromURL());
        String imageName = getImageName(card, fileExtension);
        
        Attachment image = new Attachment();
        image.ParentId = card.Id;
        image.Name = imageName;
        image.Body = RestContext.request.requestBody;
        
        return image;
    }
    
    private static String getImageName(Scanned_Business_Card__c card, String fileExtension) {
        String imageName;
        if (card.Last_Name__c != null) imageName = card.Last_Name__c + '_';
        Attachment_Settings__c attachmentSettings = getAttachmentSettings();
        imageName += attachmentSettings.Business_Card_Suffix__c + '.' + fileExtension;
        
        return imageName;
    }

}