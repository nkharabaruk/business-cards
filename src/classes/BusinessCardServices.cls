public class BusinessCardServices {
	
    public static List<Scanned_Business_Card__c> selectCardsByAccountName (Set<String> accountNames) {
        return [SELECT Account_Name__c 
                FROM Scanned_Business_Card__c 
                WHERE Account_Name__c IN : accountNames 
                AND Account__c = null];
    }
    
    public static List<Scanned_Business_Card__c> selectCardsByContactName (Set<String> contactFirstNames, Set<String> contactLastNames) {
        return [SELECT First_Name__c, Last_Name__c 
                FROM Scanned_Business_Card__c 
                WHERE First_Name__c  IN : contactFirstNames AND Last_Name__c IN : contactLastNames
                AND Contact__c = null];
    }

    public static List<Scanned_Business_Card__c> filterCardsWithChangedAccountNames(List<Scanned_Business_Card__c> cards, Map<Id, Scanned_Business_Card__c> cardsOld) {
        List<Scanned_Business_Card__c> cardsWithChangedAccount = new List<Scanned_Business_Card__c>();
        for (Scanned_Business_Card__c card : cards) { 
            if (cardsOld != null) {
                if (cardsOld.get(card.Id).Account_Name__c == card.Account_Name__c) continue;
            }
            cardsWithChangedAccount.add(card);
        }
        return cardsWithChangedAccount;
    }
    
    public static List<Scanned_Business_Card__c> filterCardsWithChangedFirstOrLastNames(List<Scanned_Business_Card__c> cards, Map<Id, Scanned_Business_Card__c> cardsOld) {
        List<Scanned_Business_Card__c> cardsWithChangedFirstOrLastNames = new List<Scanned_Business_Card__c>();
        for (Scanned_Business_Card__c card : cards) { 
            if (cardsOld != null) {
                if (cardsOld.get(card.Id).First_Name__c == card.First_Name__c
                   && cardsOld.get(card.Id).Last_Name__c == card.Last_Name__c) continue;
            }
            cardsWithChangedFirstOrLastNames.add(card);
        }
        return cardsWithChangedFirstOrLastNames;
    }
    
    public static void assignAccountToCard(List<Scanned_Business_Card__c> cards) {
        Set<String> accountNames = new Set<String>(); 
        for (Scanned_Business_Card__c card : cards) {
            accountNames.add(card.Account_Name__c);
        }
        Map<String, Id> accountNameToAccountId = AccountServices.getAccountNameToAccountId(
            AccountServices.selectAccountsByName(accountNames));
        assignAccountToCard(cards, accountNameToAccountId, false);
    }
    
    public static void assignAccountToCard(Map<String, Id> accountNameToAccountId) {
        List<Scanned_Business_Card__c> accountCards = selectCardsByAccountName(accountNameToAccountId.keySet());
        assignAccountToCard(accountCards, accountNameToAccountId, true);
    }
    
    public static void assignAccountToCard(List<Scanned_Business_Card__c> cards, Map<String, Id> accountNameToAccountId, Boolean doUpdateCards) {
        
        if (cards == null || accountNameToAccountId == null) return;
        for (Scanned_Business_Card__c card : cards) {
            card.Account__c = accountNameToAccountId.get(card.Account_Name__c); 
        }
        if (doUpdateCards) {
            update cards;
        }
    }
    
    public static void assignContactToCard(List<Scanned_Business_Card__c> cards) {
        if (cards.isEmpty()) return;
        Set<String> contactFirstNames = new Set<String>(); 
        for (Scanned_Business_Card__c card : cards) {
            contactFirstNames.add(card.First_Name__c);
        }
        Set<String> contactLastNames = new Set<String>(); 
        for (Scanned_Business_Card__c card : cards) {
            contactLastNames.add(card.Last_Name__c);
        }
        Map<String, Id> contactFirstNameToContactId = ContactServices.getContactFirstNameToContactId(
            ContactServices.selectContactsByFirstName(contactFirstNames));
        Map<String, Id> contactLastNameToContactId = ContactServices.getContactLastNameToContactId(
            ContactServices.selectContactsByLastName(contactLastNames));
        assignContactToCard(cards, contactFirstNameToContactId, contactLastNameToContactId, false);
    }
    
    public static void assignContactToCard(Map<String, Id> contactFirstNameToContactId, Map<String, Id> contactLastNameToContactId) {
        List<Scanned_Business_Card__c> contactCards = selectCardsByContactName(contactFirstNameToContactId.keySet(), contactLastNameToContactId.keySet());
        assignContactToCard(contactCards, contactFirstNameToContactId, contactLastNameToContactId, true);
    }
    
    public static void assignContactToCard(List<Scanned_Business_Card__c> cards, Map<String, Id> contactFirstNameToContactId, Map<String, Id> contactLastNameToContactId, Boolean doUpdateCards) {
        if (cards == null || contactFirstNameToContactId == null || contactLastNameToContactId == null) return;
        for (Scanned_Business_Card__c card : cards) {
            if (contactFirstNameToContactId.get(card.First_Name__c) == contactLastNameToContactId.get(card.Last_Name__c)) {
                card.Contact__c = contactFirstNameToContactId.get(card.First_Name__c);
            }
        }
        if (doUpdateCards) {
            update cards;
        }
    }
}