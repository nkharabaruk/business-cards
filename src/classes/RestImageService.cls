@RestResource(urlMapping='/card/*/image')
global class RestImageService {
    
    @HttpGet 
    global static void GET() {
        ImageServices.handleGetImageRequest();        
    }
    
    @HttpPost
    global static void POST() {
        ImageServices.handlePostImageRequest(); 
    }
}