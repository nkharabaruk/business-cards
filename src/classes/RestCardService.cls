@RestResource(urlMapping='/card')
global class RestCardService {
    
    @HttpGet 
    global static void GET() {
        BusinessCardServices.handleGetCardRequest();
    }
    
    @HttpPost
    global static void POST(String fullName, String accountName, String title, String status, String createdById) {
        BusinessCardServices.handlePostCardRequest(fullName, accountName, title, status, createdById);
    }
   
    @HttpPut
    global static void PUT(String fullName, String accountName, String title, String status, String createdById) {
        BusinessCardServices.handlePutCardRequest(fullName, accountName, title, status, createdById);
    }
}