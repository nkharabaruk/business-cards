@isTest
public class AccountTriggerTest {

    static testMethod void testAccountLookupAfterAccountInsert() {
        Scanned_Business_Card__c card = new Scanned_Business_Card__c(Account_Name__c = 'Jane Austen', Created_By_Id__c = 'someId');
        insert card;
        Account account = new Account(Name = 'Jane Austen');
        
        test.startTest();
            try {
                insert account;
                System.assert( true );
            } catch( Exception e ) {
                System.assert( false );
            }
        test.stopTest();
        
        card = [SELECT Account__c FROM Scanned_Business_Card__c WHERE Id = :card.Id];
        
        System.assertEquals(account.Id, card.Account__c);
    }
    
    static testMethod void testAccountLookupAfterAccountUpdate() {
        Account account = new Account(Name = 'Jane Austen (will be edited)');
        insert account;
        Scanned_Business_Card__c card = new Scanned_Business_Card__c(Account_Name__c = 'Jane Austen', Created_By_Id__c = 'someId');
        insert card;

        System.assertNotEquals(account.Id, card.Account__c);
        account.Name = 'Jane Austen';
        
        test.startTest();
            try {
                update account;
                System.assert( true );
            } catch( Exception e ) {
                System.assert( false );
            }
        test.stopTest();
        
        card = [SELECT Account__c FROM Scanned_Business_Card__c WHERE Id = :card.Id];
        
        System.assertEquals(account.Id, card.Account__c);
    }
}