@RestResource(urlMapping='/card')
global class CardService {
    
    @HttpGet 
    global static ResponseHandler GET() {
        ResponseHandler response = new ResponseHandler();
        List<Scanned_Business_Card__c> returnCards = getCardsByUserId();
        
        if (!returnCards.isEmpty()) {
            response.Status = Label.Success_Status;
            
            List<Map<String, String>> objects = new List<Map<String, String>>();
            for (Scanned_Business_Card__c card : returnCards) {
                Map<String, String> objectFields = new Map<String, String>();
                objectFields.put('Id', card.Id);
                objectFields.put('Full_Name__c', card.Full_Name__c);
                objectFields.put('Account_Name__c', card.Account_Name__c);
                objectFields.put('Title__c', card.Title__c);
                objectFields.put('Status__c', card.Status__c);
                objects.add(objectFields);
            }
            
            response.Data = objects;
            response.Message = Label.Success_Status + ' : ' + Label.Found_Cards;
        } else {
            response.Status = Label.Error_Status;
            response.Message = Label.Not_Found_Cards;
        } 
        
        return response;
    }
    
    public static Scanned_Business_Card__c getCardById(String Id) {
        Scanned_Business_Card__c result;
        
        try {
            result = [ Select Last_name__c, Full_Name__c, Account_Name__c, Title__c, Status__c from Scanned_Business_Card__c where Id = :Id ][0];
        } catch (System.QueryException e) {
            throw new ResponseHandler.NoRecordMatchException('Unable to find record maching Id : ' + Id);
        }
        
        return result;
    }
    
    private static List<Scanned_Business_Card__c> getCardsByUserId() {
        String userId = RestContext.request.params.get('userId');
        List<Scanned_Business_Card__c> result;
        
        try {
            result = [ Select Full_Name__c, Account_Name__c, Title__c, Status__c from Scanned_Business_Card__c where Created_By_Id__c = :userId ];
        } catch (System.QueryException e) {
            throw new ResponseHandler.NoRecordMatchException('Unable to find records maching userId : ' + userId);
        }
        
        return result;
    }
    
    @HttpPost
    global static ResponseHandler POST(String fullName, String accountName, String title, String status, String createdById) {
        Scanned_Business_Card__c card =  createCard (fullName.trim(), accountName, title, status, createdById);
        ResponseHandler response = new ResponseHandler();
        
        try {
            insert card;
            List<Map<String, String>> objects = new List<Map<String, String>>();
            Map<String, String> objectFields = new Map<String, String>();
            objectFields.put('Id', card.Id);
            objectFields.put('Full_Name__c', card.Full_Name__c);
            objectFields.put('Account_Name__c', card.Account_Name__c);
            objectFields.put('Title__c', card.Title__c);
            objectFields.put('Status__c', card.Status__c);
            objects.add(objectFields);
            
            response.Data = objects;
            response.Status = Label.Success_Status;
            response.Message = Label.Success_Status + ' : ' + Label.Found_Cards;
        } catch (DmlException e) {
            response.Status = Label.Error_Status;
            response.Message = e.getMessage();
        }
        
        return response;   
    }
    
    public static boolean isNotNullOrEmpty(string str) {
        return str!=null || !String.isBlank(str); 
    }
    
    private static Scanned_Business_Card__c createCard (String fullName, String accountName, String title, String status, String createdById) {
        if (isNotNullOrEmpty(fullName) && isNotNullOrEmpty(accountName) && isNotNullOrEmpty(title) && isNotNullOrEmpty(status) && isNotNullOrEmpty(createdById)) {
            Scanned_Business_Card__c newCard = new Scanned_Business_Card__c();
            if(fullName.contains(' ')) {
                String firstName = fullName.split(' ')[0];
                String lastName = fullName.split(' ')[1];    
                newCard.First_Name__c = firstName;
                newCard.Last_Name__c = lastName;
            } else {
                System.Debug('Invalid name provided.');
            }
            newCard.Account_Name__c = accountName;
            newCard.Title__c = title;
            newCard.Status__c = status;
            newCard.Created_By_Id__c = createdById;
            
            return newCard;         
        } else  {
            System.Debug('Required field values are not provided here');
            return null;
        }
    }
   
    @HttpPut
    global static ResponseHandler PUT(String fullName, String accountName, String title, String status, String createdById) {
        Scanned_Business_Card__c card = getCardById(RestContext.request.params.get('Id'));
        card =  updateCard (card, fullName, accountName, title, status, createdById);
        ResponseHandler response = new ResponseHandler();
        
        try {
            update card;
            List<Map<String, String>> objects = new List<Map<String, String>>();
            Map<String, String> objectFields = new Map<String, String>();
            objectFields.put('Id', card.Id);
            objectFields.put('Full_Name__c', card.Full_Name__c);
            objectFields.put('Account_Name__c', card.Account_Name__c);
            objectFields.put('Title__c', card.Title__c);
            objectFields.put('Status__c', card.Status__c);
            objects.add(objectFields);
            
            response.Data = objects;
            response.Status = Label.Success_Status;
            response.Message = Label.Success_Status + ' : ' + Label.Found_Cards;
        } catch (DmlException e) {
            response.Status = Label.Error_Status;
            response.Message = e.getMessage();
        }
        
        return response;   
    }
    
    private static Scanned_Business_Card__c updateCard (Scanned_Business_Card__c returnCard, String fullName, String accountName, String title, String status, String createdById) {
        if (isNotNullOrEmpty(fullName)) {
            if(fullName.contains(' ')) {
                String firstName = fullName.split(' ')[0];
                String lastName = fullName.split(' ')[1];    
                returnCard.First_Name__c = firstName;
                returnCard.Last_Name__c = lastName;
            } else {
                System.Debug('Invalid name provided.');
            }
        }
        if (isNotNullOrEmpty(accountName)) returnCard.Account_Name__c = accountName;
        if (isNotNullOrEmpty(title)) returnCard.Title__c = title;
        if (isNotNullOrEmpty(status)) returnCard.Status__c = status;
        if (isNotNullOrEmpty(createdById)) returnCard.Created_By_Id__c = createdById;
        
        return returnCard;
    }
}