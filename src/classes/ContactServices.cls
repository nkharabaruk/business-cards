public class ContactServices {

    public static List<Contact> selectContactsByFirstName(Set<String> contactFirstNames) {
        return [SELECT FirstName, LastName, LastModifiedDate
                FROM Contact 
                WHERE FirstName IN :contactFirstNames
                ORDER BY LastModifiedDate DESC];
    }
    
    public static List<Contact> selectContactsByLastName(Set<String> contactLastNames) {
        return [SELECT FirstName, LastName, LastModifiedDate
                FROM Contact 
                WHERE LastName IN :contactLastNames
                ORDER BY LastModifiedDate DESC];
    }
    
    public static List<Contact> filterContactsWithChangedNames(List<Contact> contacts, Map<Id, Contact> contactsOld) {
        List<Contact> contactsWithChangedName = new List<Contact>();
        for (Contact contact : contacts) { 
            if (contactsOld != null) {
                if (contactsOld.get(contact.Id).FirstName == contact.FirstName
                   && contactsOld.get(contact.Id).LastName == contact.LastName) continue;
            }
            contactsWithChangedName.add(contact);
        }
        return contactsWithChangedName;
    }
    
    public static Map<String, Id> getContactFirstNameToContactId (List<Contact> contacts) {
        Map<String, Id> contactFirstNameToContactId = new Map<String, Id>();
        for (Contact contact : contacts) {
            if (contactFirstNameToContactId.get(contact.FirstName) != null) continue;
            contactFirstNameToContactId.put(contact.FirstName, contact.Id);
        }
        return contactFirstNameToContactId;
    }
    
    public static Map<String, Id> getContactLastNameToContactId (List<Contact> contacts) {
        Map<String, Id> contactLastNameToContactId = new Map<String, Id>();
        for (Contact contact : contacts) {
            if (contactLastNameToContactId.get(contact.LastName) != null) continue;
            contactLastNameToContactId.put(contact.LastName, contact.Id);
        }
        return contactLastNameToContactId;
    }
}