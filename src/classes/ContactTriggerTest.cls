@isTest
public class ContactTriggerTest {

    static testMethod void testContactLookupAfterContactInsert() {
        Scanned_Business_Card__c card = new Scanned_Business_Card__c(First_Name__c = 'Jane', Last_Name__c = 'Austen',Created_By_Id__c = 'someId');
        insert card;
        Contact contact = new Contact(FirstName = 'Jane', LastName = 'Austen');
        
        test.startTest();
            try {
                insert contact;
                System.assert( true );
            } catch( Exception e ) {
                System.assert( false );
            }
        test.stopTest();
        
        card = [SELECT Contact__c FROM Scanned_Business_Card__c WHERE Id = :card.Id];
        
        System.assertEquals(Contact.Id, card.Contact__c);
    }
    
    static testMethod void testContactLookupAfterContactUpdate() {
        Contact contact = new Contact(FirstName = 'Not Jane', LastName = 'Austen');
        insert contact;
        Scanned_Business_Card__c card = new Scanned_Business_Card__c(First_Name__c = 'Jane', Last_Name__c = 'Austen', Created_By_Id__c = 'someId');
        insert card;

        System.assertNotEquals(contact.Id, card.Contact__c);
        contact.FirstName = 'Jane';
        
        test.startTest();
            try {
                update contact;
                System.assert( true );
            } catch( Exception e ) {
                System.assert( false );
            }
        test.stopTest();
        
        card = [SELECT Contact__c FROM Scanned_Business_Card__c WHERE Id = :card.Id];
        
        System.assertEquals(contact.Id, card.Contact__c);
    }
}