@isTest
public class RestCardServiceTest {
    
    static testMethod void testGET() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        Scanned_Business_Card__c card = new Scanned_Business_Card__c(Last_Name__c = 'Vasylenko', Created_By_Id__c = '123'); 
        insert card;
        
        req.requestURI = 'https://business-cards-developer-edition.eu11.force.com/services/apexrest/business_cards/card?userId=1';  
        req.httpMethod = 'GET';
        
        test.startTest();
        RestCardService.GET();
        test.stopTest();
        
        String testBlob = res.responseBody.toString();
        
        System.assertEquals(true, testBlob.contains(card.Last_Name__c));
        System.assertEquals(true, testBlob.contains(card.Created_By_Id__c));
        
        /*System.assertEquals('true', results.success);
        System.assertEquals(10, results.records.size());
        System.assertEquals('Query executed successfully.', results.message);*/
    }
    
    static testMethod void testPOST() {}

    static testMethod void testPUT() {}    
}