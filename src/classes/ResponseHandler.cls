global class ResponseHandler {
    
    public String Status {get; set;}
    //public List<sObject> Data {get;set;}
    public String Message {get;set;} 
    public List<Map<String, String>> Data {get;set;}
    
    public class NoRecordMatchException extends Exception {}
    public class InvalidFileException extends Exception {}
}