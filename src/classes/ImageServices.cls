public class ImageServices {
    
    public static void handleGetImageRequest() {
        RestResponse response = RestContext.response;
        
        try {
        	Attachment image = getImageByCardId();
            
            response.statusCode = 200;
            response.headers.put('Content-Type', image.contentType);
            response.headers.put('Content-Disposition', 'attachment; filename="' + image.Name + '"');
            response.responseBody = image.Body;
        } catch (Exception e) {
            response.statusCode = 404; 
        }
    }
    
    public static void handlePostImageRequest() {
        ResponseHandler handler = new ResponseHandler();
        RestResponse response = RestContext.response;
        
        try {
        	Attachment image = createImage();
            insert image;
            handler.Status = Label.Success_Status;
            handler.Message = Label.Success_Status + ' : ' + Label.Found_Image;
            
            response.statusCode = 200;
            response.headers.put('Content-Type', 'application/json');
            response.responseBody = Blob.valueOf(JSON.serialize(handler));           
        } catch (Exception e) {
            handler.Status = Label.Error_Status;
            handler.Message = Label.Not_Found_Cards;  
            
            response.statusCode = 404;
        }
        
        if (response.statusCode == 200) removeOtherImages(getCardIdFromURL());
    }
    
    private static Attachment_Settings__c getAttachmentSettings() {
        Attachment_Settings__c attachmentSettings = Attachment_Settings__c.getInstance();
        return attachmentSettings;
    }
    
    private static Attachment getImageByCardId() {
		String cardId = getCardIdFromURL();
        List<Attachment> images;
        try {
            images = [SELECT Name, contentType, Body FROM Attachment WHERE ParentId = : cardId]; 
        } catch(System.QueryException e) {
            throw new ResponseHandler.NoRecordMatchException('Unable to find image with cardId : ' + cardId);
        } 
        if (images.isEmpty()) throw new ResponseHandler.NoRecordMatchException('Unable to find image with cardId : ' + cardId);
        
        return images[0];        
    }
    
    private static Id getCardIdFromURL() {
        String cardId = RestContext.request.requestURI.substringBetween('card/', '/image');
        if (!ResponseHandler.isNotNullOrEmpty(cardId)) throw new ResponseHandler.NoRecordMatchException('Unable to find records maching Id : ' + cardId);
        if (!(cardId instanceOf Id) || !cardId.startsWith(Scanned_Business_Card__c.sObjectType.getDescribe().getKeyPrefix())) {
            throw new ResponseHandler.NoRecordMatchException('Unable to find records maching Id : ' + cardId);
        }
        return cardId;
    }
    
    private static void removeOtherImages(Id cardId) {
        List<Attachment> images = [SELECT Id FROM Attachment WHERE ParentId = : cardId ORDER BY CreatedDate DESC];
        images.remove(0);
        if (images.size() == 1) return;
        
        try {
            delete images;
        } catch (DmlException e) {
            throw new ResponseHandler.NoRecordMatchException('Unable to find records maching Id : ' + cardId);
        }
    }
    
    private static Attachment createImage() {
        Scanned_Business_Card__c card = BusinessCardServices.getCardById(getCardIdFromURL());
        String contentType = RestContext.request.headers.get('Content-Type');
        if (contentType == null) contentType = 'image/jpeg';
        String imageName = getImageName(card, contentType);
        
        Attachment image = new Attachment();
        image.ParentId = card.Id;
        image.Name = imageName;
        image.contentType = contentType;
        image.Body = RestContext.request.requestBody;
        
        return image;
    }
    
    private static String getImageName(Scanned_Business_Card__c card, String contentType) {
        String imageName;
        if (card.Last_Name__c != null) imageName = card.Last_Name__c + '_';
        Attachment_Settings__c attachmentSettings = getAttachmentSettings();
        String fileExtension = getFileExtension(contentType);
        imageName += attachmentSettings.Business_Card_Suffix__c + '.' + fileExtension;
        
        return imageName;
    }
    
    private static String getFileExtension(String contentType) {
        if (contentType.split('/')[0] != 'image') {
            throw new ResponseHandler.InvalidFileException('Invalid file. You cannot upload: ' + contentType.split('/')[0]);
        } 
        return contentType.split('/')[1];
    }
}