# My project's README
Application for Business Cards management. 
Custom objects hold full cards information with optional image. 
Account and Contact lookup fills automatically by name with trigger on Card update or Account and Contact update or create.
External open API is provided with messages and error code. 
It includes GET and POST methods for Cards and Images, PUT - for Cards.